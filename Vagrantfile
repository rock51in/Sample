# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.

  config.vm.box = "hemangajmera/centos7-gnome"
  config.vm.synced_folder ".", "/vagrant", type:"virtualbox"
  config.vm.network "public_network"
  #config.vm.network :forwarded_port, guest: 80, host: 8888
  config.vbguest.iso_path = "VBoxGuestAdditions.iso"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
 
    # Customize the amount of memory on the VM:
    vb.memory = "4096"
    vb.name = "dev-box"
    vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    # vb.customize ["modifyvm", :id, "--draganddrop", "bidirectional"]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    
    # Setting up password for vagrant user and root
    # echo "vagrant" | passwd --stdin vagrant
    # echo "vagrant" | passwd --stdin root
    
    # Setting up hostname
    sudo hostnamectl set-hostname tibco.local

    # Increasing timeout value due to CGI proxy scanning
    grep timeout /etc/yum.conf > /dev/null || echo timeout=600 | sudo tee -a /etc/yum.conf    
    
    # Installing docker as per https://docs.docker.com/engine/installation/linux/centos/
    sudo yum makecache fast
    sudo yum -y update
    sudo yum -y remove docker docker-common container-selinux docker-selinux docker-engine
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum makecache fast
    sudo yum -y install docker-ce
    sudo groupadd docker
    sudo usermod -aG docker vagrant
    newgrp - docker; newgrp - vagrant # This is needed to add docker group in current session. See http://superuser.com/questions/272061/reload-a-linux-users-group-assignments-without-logging-out
    sudo systemctl start docker
    docker ps
    sudo systemctl enable docker
    sudo sysctl -w net.ipv4.ip_forward=1
    
    # Install nginx
    sudo yum -y install epel-release
    sudo yum -y install nginx
    sudo systemctl enable nginx
    sudo semanage permissive -a httpd_t

    
    # Install Nexus
    docker pull sonatype/nexus3
    docker rm -f nexus || echo "Nexus docker does not exists. No Action needed"
    docker run -d -p 48001:8081 --name nexus --restart=unless-stopped -e NEXUS_CONTEXT=nexus sonatype/nexus3
    sudo cp /vagrant/dockers/nexus/nginx.conf /etc/nginx/default.d/nexus.conf
    
    # Install Maven
    sudo yum -y install java-1.8.0-openjdk-devel-debug java-1.8.0-openjdk-devel
    curl --silent --output apache-maven-3.3.9-bin.tar.gz http://www-eu.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
    sudo tar zxvf apache-maven-3.3.9-bin.tar.gz -C /opt
    rm apache-maven-3.3.9-bin.tar.gz
    
    mkdir ~/.m2 || echo ".m2 dir already exist"
    cp /vagrant/maven_settings.xml ~/.m2/settings.xml
    cp /vagrant/bash_profile ~/.bash_profile
    . ~/.bash_profile
    
    sudo yum-config-manager --add-repo https://pkg.jenkins.io/redhat/jenkins.repo
    sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
    sudo yum -y install jenkins
    sudo systemctl start jenkins
    sudo cp /vagrant/dockers/jenkins/nginx.conf /etc/nginx/default.d/jenkins.conf

    # Start nginx
    sudo systemctl stop nginx
    sudo systemctl start nginx

    # Other useful stuff
    sudo yum -y install git-gui tree dos2unix unix2dos


    # Install TIBCO
    /vagrant/tibco_install.sh

  SHELL
end
