 #!/bin/bash
echo "Deploying Maven Plugin to Nexus Repository"
REPO="-DrepositoryId=nexus -Durl=http://localhost/nexus/repository/maven-releases/"
mvn deploy:deploy-file -Dfile=bw6-maven-plugin-1.2.0.jar -DpomFile=pom.xml $REPO

mvn deploy:deploy-file -Dfile=com.tibco.bw.palette.shared_6.1.100.003.jar -DgroupId=com.tibco.plugins -DartifactId=com.tibco.bw.palette.shared -Dversion=6.1.100 -Dpackaging=jar $REPO

mvn deploy:deploy-file -Dfile=com.tibco.xml.cxf.common_1.3.200.001.jar -DgroupId=com.tibco.plugins -DartifactId=com.tibco.xml.cxf.common -Dversion=1.3.200 -Dpackaging=jar $REPO

mvn deploy:deploy-file -Dfile=fabric8/fabric8-maven-plugin-2.2.102.jar -DpomFile=fabric8/pom.xml $REPO

#EOF
