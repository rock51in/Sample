# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

M2_Home=/opt/apache-maven-3.3.9
PATH=$PATH:$M2_Home/bin:$HOME/.local/bin:$HOME/bin

export PATH
